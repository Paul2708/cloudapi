import de.paul2708.api.CloudAPI;
import de.paul2708.api.request.Request;
import de.paul2708.api.request.RequestHandler;
import de.paul2708.api.request.Response;
import de.paul2708.api.stats.Statistic;
import de.paul2708.api.stats.StatisticManager;
import de.paul2708.api.stats.StatisticSetup;
import de.paul2708.api.stats.Time;
import de.paul2708.api.update.Upgrade;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.common.command.CommandType;
import de.paul2708.common.nick.Nick;
import de.paul2708.common.rank.Rank;
import de.paul2708.common.server.ServerData;
import de.paul2708.common.stats.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.UUID.fromString;

/**
 * Created by Paul on 05.09.2016.
 */
public class Example implements RequestHandler {

    private static final UUID PLAYER = fromString("bc3b3f26-e70c-4c66-bdec-5bd7246967bc");

    public static void main(String[] args) {
        // Instance
        CloudAPI api = new CloudAPI("Example");

        // Request Handler
        api.addRequestHandler(new Example());

        // Connect
        api.connect("localhost", 2000);

        // Request
        Request request = new Request.Builder()
                .identifier("example")
                .add(Information.Type.TOKENS, PLAYER)
                .add(Information.Type.ONLINE_PLAYERS)
                .add(Information.Type.PLAYER_RANK, PLAYER)
                .add(Information.Type.PLAY_TIME, PLAYER)
                .add(Information.Type.SERVER_DATA, "Lobby-01")
                .add(Information.Type.AVAILABLE_SERVER, "BedWars")
                .add(Information.Type.INGAME_SERVER, "BedWars")
                .add(Information.Type.NICK, PLAYER)
                /*.handle(response -> {
                    // Inner handle
                    System.out.println("Tokens: " + response.get(0));
                })*/
                .build();
        request.send();

        // Upgrade
        Upgrade upgrade = new Upgrade.Builder()
                .add(Update.Type.TOKENS, PLAYER, +10)
                .add(Update.Type.COMMAND, PLAYER, CommandType.BAN, "BadPlayer perma &cDo not team.")
                .add(Update.Type.COMMAND, PLAYER, CommandType.TEAMCHAT, "Hallo Leute! :^)")
                .add(Update.Type.SEND, PLAYER, "Lobby-01")
                .add(Update.Type.MOTD, "Lobby-01", "Das ist die neue Lobby-01 Motd.")
                .build();
        upgrade.send();

        // Cache
        Rank rank = api.getCache().get(Information.Type.PLAYER_RANK, PLAYER);
        int tokens = api.getCache().get(Information.Type.TOKENS, PLAYER);
        // [...]

        // Stats
        setupStats(api);
    }

    // Request handler
    @Override
    public void onHandle(Response response) {
        if (response.getReason().equalsIgnoreCase("example")) {
            int tokens = response.get(0);
            int count = response.get(1);
            Rank rank = response.get(2);
            int time = response.get(3);
            ServerData data = response.get(4);
            ServerData group = response.get(5);
            ServerData[] array = response.get(6);
            Nick nick = response.get(7);

            System.out.println("Tokens: " + tokens + " Tokens");
            System.out.println("Count: " + count + " Player");
            System.out.println("Rank: " + rank.getName() + "(" + rank.getPermissionLevel() + ")");
            System.out.println("Time: " + time + " min");
            System.out.println("Lobby-Server: " + data.getName());
            System.out.println("Gefundener BW-Server: " + group.getName());
            System.out.println("Gefundene Ingame-Server: " + array.length);
            System.out.println("Nick-Name: " + nick.getName());
        }
    }

    // Stats handling
    private static void setupStats(CloudAPI api) {
        // Setup
        StatisticSetup setup = new StatisticSetup()
                // Name
                .name("Example")
                // Server tag
                .tag("lobby")
                // Categories
                .category(new Category("Kill", "kills", Category.Type.INTEGER, 200, "0"))
                .category(new Category("Death", "deaths", Category.Type.INTEGER, 200, "0"))
                .category(new Category("Point", "points", Category.Type.INTEGER, 200, "100"))
                // Command
                .command(data -> {
                    List<String> list = new ArrayList<>();
                    list.add("[Example] Kills: " + data.get("Kill"));
                    list.add("[Example] Tode: " + data.get("Death"));
                    list.add("[Example] Points: " + data.get("Point"));
                    return list;
                });

        // Register statistic manager with setup
        StatisticManager manager = api.getStatisticManager();
        manager.applySetup(setup);
        manager.register();

        // Get statistics
        Statistic stats = manager.getStatistic(PLAYER);

        // Request stats
        stats.request();

        // Update
        stats.update("Kill", 1);
        stats.update("Point", 10);

        // Set
        stats.set("Point", 100);

        // Get
        int kills = stats.get("Kill", Time.LOCAL);
        int globalKills = stats.get("Kill", Time.GLOBAL);

        // Push
        stats.push();
    }
}
