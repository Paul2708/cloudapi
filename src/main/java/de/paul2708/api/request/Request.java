package de.paul2708.api.request;

import de.paul2708.api.CloudAPI;
import de.paul2708.common.api.Information;
import de.paul2708.common.packet.api.ApiInformationPacket;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 26.12.2016.
 */
public class Request {

    private static Map<Integer, Request> cache = new ConcurrentHashMap<>();
    private static int currentId = 0;

    private String reason;
    private Information[] information;
    private RequestHandler handler;
    private int id;

    public Request(String reason, Information[] information, RequestHandler handler) {
        this.reason = reason;
        this.information = information;
        this.handler = handler;
        this.id = currentId;

        Request.cache.put(currentId, this);
        Request.currentId++;
    }

    public void send() {
        ApiInformationPacket packet = new ApiInformationPacket(id, reason, information);

        CloudAPI.getInstance().getClient().send(packet);
    }

    public RequestHandler getHandler() {
        return handler;
    }

    public static Request getById(int id) {
        for (Integer ids : cache.keySet()) {
            if (ids == id) {
                return cache.get(ids);
            }
        }

        return null;
    }

    public static class Builder {

        private String reason;
        private Map<Integer, Information> map;
        private RequestHandler handler;

        private int index;

        public Builder() {
            this.reason = "default";
            this.map = new HashMap<>();
            this.index = 0;
        }

        public Builder identifier(String reason) {
            this.reason = reason;
            return this;
        }

        public Builder add(Information.Type information) {
            return add(information, null);
        }

        public Builder add(Information.Type information, Object argument) {
            if (argument != null && !information.getArgument().isInstance(argument)) {
                throw new IllegalArgumentException("False argument for " + information);
            }

            Information info = new Information(information, argument);
            map.put(index, info);
            index++;
            return this;
        }

        public Builder handle(RequestHandler handler) {
            this.handler = handler;
            return this;
        }

        public Request build() {
            Information[] information = new Information[index];
            for (int i = 0; i < index; i++) {
                information[i] = getById(i);
            }

            return new Request(reason, information, handler);
        }

        private Information getById(int id) {
            for (Integer integer : map.keySet()) {
                if (integer == id) {
                    return map.get(integer);
                }
            }

            return null;
        }
    }
}
