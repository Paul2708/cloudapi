package de.paul2708.api.request;

import de.paul2708.common.api.Information;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 30.12.2016.
 */
public class Cache {

    private List<Information> informations;

    public Cache() {
        this.informations = new CopyOnWriteArrayList<>();
    }

    public void add(Information information) {
        for (Information all : informations) {
            if (all.getType() == information.getType()) {
                if (all.getArgument() == null && information.getArgument() == null) {
                    informations.remove(all);
                    informations.add(information);
                    return;
                } else if (all.getArgument().equals(information.getArgument())){
                    informations.remove(all);
                    informations.add(information);
                    return;
                }
            }
        }

        this.informations.add(information);
    }

    public <T> T get(Information.Type type) {
        return get(type, null);
    }

    public <T> T get(Information.Type type, Object argument) {
        for (Information information : informations) {
            if (information.getType() == type) {
                if (argument == null || argument.equals(information.getArgument())) {
                    return information.getResponse();
                }
            }
        }

        return null;
    }
}
