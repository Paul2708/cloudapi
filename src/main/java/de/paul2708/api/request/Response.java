package de.paul2708.api.request;

import de.paul2708.common.api.Information;

/**
 * Created by Paul on 26.12.2016.
 */
public class Response {

    private String reason;
    private Information[] information;

    public Response(String reason, Information[] information) {
        this.reason = reason;
        this.information = information;
    }

    public <T> T get(int index) {
        return information[index].getResponse();
    }

    public String getReason() {
        return reason;
    }

    public Information[] getInformation() {
        return information;
    }
}
