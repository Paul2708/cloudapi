package de.paul2708.api.request;

/**
 * Created by Paul on 26.12.2016.
 */
public interface RequestHandler {

    void onHandle(Response response);
}
