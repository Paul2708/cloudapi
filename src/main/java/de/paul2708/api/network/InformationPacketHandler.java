package de.paul2708.api.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.api.CloudAPI;
import de.paul2708.api.request.Request;
import de.paul2708.api.request.Response;
import de.paul2708.common.api.Information;
import de.paul2708.common.packet.api.ApiInformationPacket;

/**
 * Created by Paul on 26.12.2016.
 */
public class InformationPacketHandler implements PacketListener {

    @PacketHandler
    public void onInformation(Session session, ApiInformationPacket packet) {
        // Information
        Information[] array = packet.getInformation();
        Response response = new Response(packet.getReason(), array);

        for (Information information : array) {
            CloudAPI.getInstance().getCache().add(information);
        }

        if (CloudAPI.getInstance().getHandler() != null) {
            if (Request.getById(packet.getId()).getHandler() != null) {
                Request.getById(packet.getId()).getHandler().onHandle(response);
            } else {
                CloudAPI.getInstance().getHandler().onHandle(response);
            }
        } else {
            if (Request.getById(packet.getId()).getHandler() != null) {
                Request.getById(packet.getId()).getHandler().onHandle(response);
            }
        }
    }
}
