package de.paul2708.api.network;

import de.jackwhite20.cascade.shared.protocol.listener.PacketHandler;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.session.Session;
import de.paul2708.api.CloudAPI;
import de.paul2708.common.packet.stats.StatsCommandPacket;

import java.util.ArrayList;

/**
 * Created by Paul on 31.12.2016.
 */
public class StatsPacketHandler implements PacketListener {

    @PacketHandler
    public void onCommand(Session session, StatsCommandPacket packet) {
        // Command
        ArrayList<String> list = new ArrayList<>();
        list.addAll(CloudAPI.getInstance().getStatisticManager().getListener().onComamnd(packet.getData()));
        StatsCommandPacket response = new StatsCommandPacket(packet.getUuid(), packet.getData(), list);
        session.send(response);
    }
}
