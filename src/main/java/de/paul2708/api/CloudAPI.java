package de.paul2708.api;

import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.paul2708.api.network.InformationPacketHandler;
import de.paul2708.api.network.StatsPacketHandler;
import de.paul2708.api.request.Cache;
import de.paul2708.api.request.RequestHandler;
import de.paul2708.api.stats.StatisticManager;
import de.paul2708.common.network.PacketClient;
import de.paul2708.common.packet.ClientType;
import de.paul2708.common.packet.client.ClientLoginPacket;

/**
 * Created by Paul on 05.09.2016.
 */
public class CloudAPI {

    private static CloudAPI instance;

    private String usage;

    private PacketClient client;

    private Cache cache;

    private RequestHandler handler;

    private StatisticManager statisticManager;

    public CloudAPI(String usage) {
        this.usage = usage;

        this.cache = new Cache();

        this.statisticManager = new StatisticManager(this);

        CloudAPI.instance = this;
    }

    public void connect(String host, int port, PacketListener... listener) {
        this.client = new PacketClient(host, port);

        PacketListener[] totalListener = new PacketListener[listener.length + 2];
        for (int i = 0; i < listener.length; i++) {
            totalListener[i] = listener[i];
        }
        totalListener[listener.length] = new InformationPacketHandler();
        totalListener[listener.length + 1] = new StatsPacketHandler();

        client.registerListeners(totalListener);
        client.connect();
        client.send(new ClientLoginPacket(ClientType.API.getId(), usage));
    }

    public void addRequestHandler(RequestHandler handler) {
        this.handler = handler;
    }

    public PacketClient getClient() {
        return client;
    }

    public Cache getCache() {
        return cache;
    }

    public RequestHandler getHandler() {
        return handler;
    }

    public StatisticManager getStatisticManager() {
        return statisticManager;
    }

    public static CloudAPI getInstance() {
        return instance;
    }
}
