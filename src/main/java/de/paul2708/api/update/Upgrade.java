package de.paul2708.api.update;

import de.paul2708.api.CloudAPI;
import de.paul2708.common.api.Update;
import de.paul2708.common.packet.api.ApiUpdatePacket;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 29.12.2016.
 */
public class Upgrade {

    private static int currentId = 0;

    private Update[] updates;

    public Upgrade(Update... updates) {
        this.updates = updates;

        Upgrade.currentId++;
    }

    public void send() {
        ApiUpdatePacket packet = new ApiUpdatePacket(currentId, updates);

        CloudAPI.getInstance().getClient().send(packet);
    }

    public static class Builder {

        private Map<Integer, Update> map;
        private int index;

        public Builder() {
            this.map = new HashMap<>();
            this.index = 0;
        }

        public Builder add(Update.Type update, Object... arguments) {
            for (int i = 0; i < update.getArguments().length; i++) {
                if (!update.getArguments()[i].isInstance(arguments[i])) {
                    throw new IllegalArgumentException("False argument for " + update);
                }
            }

            Update info = new Update(update, arguments);
            map.put(index, info);
            index++;
            return this;
        }

        public Upgrade build() {
            Update[] updates = new Update[index];
            for (int i = 0; i < index; i++) {
                updates[i] = getById(i);
            }

            return new Upgrade(updates);
        }

        private Update getById(int id) {
            for (Integer integer : map.keySet()) {
                if (integer == id) {
                    return map.get(integer);
                }
            }

            return null;
        }
    }
}
