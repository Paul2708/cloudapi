package de.paul2708.api.stats;

import de.paul2708.api.request.Request;
import de.paul2708.common.api.Information;
import de.paul2708.common.packet.stats.StatsPushPacket;
import de.paul2708.common.packet.stats.StatsUpdatePacket;
import de.paul2708.common.stats.Category;
import de.paul2708.common.stats.StatisticData;
import de.paul2708.common.util.Util;

import java.util.*;

/**
 * Created by Paul on 16.10.2016.
 */
public class Statistic {

	private static List<Statistic> cache = new ArrayList<>();

	private StatisticManager manager;
	private UUID uuid;

	private StatisticData stats;
	private StatisticData roundStats;

	public Statistic(StatisticManager manager, UUID uuid) {
		this.manager = manager;
		this.uuid = uuid;

		this.stats = new StatisticData(uuid, manager.getName());
		this.roundStats = new StatisticData(uuid, manager.getName());

		Statistic.cache.add(this);
	}

	// Request
	public void request() {
		Request request = new Request.Builder()
				.identifier("stats_request")
				.add(Information.Type.STATISTIC_DATA, new StatisticData(this.uuid, manager.getName()))
				.handle(response -> {
					this.stats = response.get(0);
					Map<Category, Object> defaultStats = new HashMap<>();
					for (Category category : manager.getCategories()) {
						defaultStats.put(category, Util.getValueByString(category, category.getDefaultValue()));
					}
					this.roundStats.setStats(defaultStats);

					if (stats.getStats().size() == 0) {
						defaultStats = new HashMap<>();
						for (Category category : manager.getCategories()) {
							defaultStats.put(category, Util.getValueByString(category, category.getDefaultValue()));
						}

						this.stats.setStats(defaultStats);
						this.roundStats.setStats(defaultStats);
					}
				})
				.build();
		request.send();
	}

	// Push
	public void push() {
		manager.getInstance().getClient().send(new StatsPushPacket(stats));
	}

	// Update
	public void update(String key, int value) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return;
		}

		int oldValueLocal = roundStats.get(key);
		int oldValueGlobal = stats.get(key);

		stats.set(key, oldValueGlobal + value);
		roundStats.set(key, oldValueLocal + value);

		manager.getInstance().getClient().send(new StatsUpdatePacket(manager.getName(), uuid, key, oldValueGlobal + value));
	}

	public void update(String key, long value) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return;
		}

		long oldValueLocal = roundStats.get(key);
		long oldValueGlobal = stats.get(key);

		stats.set(key, oldValueGlobal + value);
		roundStats.set(key, oldValueLocal + value);

		manager.getInstance().getClient().send(new StatsUpdatePacket(manager.getName(), uuid, key, oldValueGlobal + value));
	}

	// Set
	public void set(String key, int value) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return;
		}

		stats.set(key, value);
		roundStats.set(key, value);

		manager.getInstance().getClient().send(new StatsUpdatePacket(manager.getName(), uuid, key, value));
	}

	public void set(String key, long value) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return;
		}

		stats.set(key, value);
		roundStats.set(key, value);

		manager.getInstance().getClient().send(new StatsUpdatePacket(manager.getName(), uuid, key, value));
	}

	public void set(String key, String value) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return;
		}

		stats.set(key, value);
		roundStats.set(key, value);

		manager.getInstance().getClient().send(new StatsUpdatePacket(manager.getName(), uuid, key, value));
	}

	// Get
	public <T> T get(String key, Time time) {
		Category category = getCategory(key);
		if (category == null) {
			System.out.println("The category " + key + " cannot be null");
			return null;
		}

		if (time == Time.GLOBAL) {
			return Util.getValue(category, stats.get(key));
		} else if (time == Time.LOCAL) {
			return Util.getValue(category, roundStats.get(key));
		}

		return null;
	}

	public UUID getUuid() {
		return uuid;
	}

	private Category getCategory(String name) {
		for (Category category : manager.getCategories()) {
			if (category.getName().equalsIgnoreCase(name)) {
				return category;
			}
		}

		return null;
	}

	public static Statistic getByUUID(UUID uuid) {
		for (Statistic statistic : cache) {
			if (statistic.getUuid().toString().equals(uuid.toString())) {
				return statistic;
			}
		}

		return null;
	}

}
