package de.paul2708.api.stats;

import de.paul2708.common.stats.StatisticData;

import java.util.List;

/**
 * Created by Paul on 29.10.2016.
 */
public interface CommandListener {

    List<String> onComamnd(StatisticData data);
}
