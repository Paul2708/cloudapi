package de.paul2708.api.stats;

import de.paul2708.api.CloudAPI;
import de.paul2708.common.packet.stats.StatsRegisterPacket;
import de.paul2708.common.stats.Category;
import de.paul2708.common.stats.GameStatistic;

import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
public class StatisticManager {

    private CloudAPI instance;

    private StatisticSetup setup;

    public StatisticManager(CloudAPI instance) {
        this.instance = instance;
    }

    public void applySetup(StatisticSetup setup) {
        this.setup = setup;
    }

    public void register() {
        // Check valid arguments
        if (setup.getName().equals("")) {
            System.out.println("[StatsAPI] The name cannot be null.");
            return;
        }
        if (setup.getCategories().size() == 0) {
            System.out.println("[StatsAPI] There must be categories.");
            return;
        }
        if (setup.getCommandListener() == null) {
            System.out.println("[StatsAPI] The command listener cannot be null.");
            return;
        }

        StatsRegisterPacket packet = new StatsRegisterPacket(new GameStatistic(setup.getName(),
                setup.getCategories(), setup.getTag()));
        instance.getClient().send(packet);
    }

    public Statistic getStatistic(UUID uuid) {
        return Statistic.getByUUID(uuid) == null ? new Statistic(this, uuid) : Statistic.getByUUID(uuid);
    }

    public String getName() {
        return setup.getName();
    }

    public List<Category> getCategories() {
        return setup.getCategories();
    }

    public CommandListener getListener() {
        return setup.getCommandListener();
    }

    public CloudAPI getInstance() {
        return instance;
    }
}
