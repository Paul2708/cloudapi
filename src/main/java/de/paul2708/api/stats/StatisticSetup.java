package de.paul2708.api.stats;

import de.paul2708.common.stats.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 16.10.2016.
 */
public class StatisticSetup {

    private String name;
    private String serverTag;
    private List<Category> categories;
    private CommandListener commandListener;

    public StatisticSetup() {
        this.name = "";
        this.serverTag = "";
        this.categories = new ArrayList<>();
    }

    public StatisticSetup name(String game) {
        this.name = game;
        return this;
    }

    public StatisticSetup tag(String serverTag) {
        this.serverTag = serverTag;
        return this;
    }

    public StatisticSetup category(Category category) {
        categories.add(category);
        return this;
    }

    public StatisticSetup command(CommandListener commandListener) {
        this.commandListener = commandListener;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getTag() {
        return serverTag;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public CommandListener getCommandListener() {
        return commandListener;
    }
}
